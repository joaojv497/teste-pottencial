using Pottencial.Kernel.DTO.Enum;
using Pottencial.Kernel.DTO.Request;
using Pottencial.Kernel.DTO.Response;
using Pottencial.Kernel.Model;

namespace Pottencial.Kernel.Controller;

public class OperationController
{
   
    public async Task<ClienteResponse> CriarCliente(ClienteRequest cliente)
    {
        using var context = PottencialContext.Get();
        var c = new Cliente(cliente);

        await context.SaveChangesAsync();

        return new ClienteResponse()
        {
            Id = c.Id
        };
    }
    
    public async Task<ProdutoResponse> CriarProduto(ProdutoRequest produto)
    {
        using var context = PottencialContext.Get();
        var p = new Produto(produto);

        await context.SaveChangesAsync();

        return new ProdutoResponse()
        {
            Id = p.Id
        };
    }

    public async Task<VendedorResponse> CriarVendedor(VendedorRequest vendedor)
    {
        using var context = PottencialContext.Get();
        var v = new Vendedor(vendedor);

        await context.SaveChangesAsync();

        return new VendedorResponse()
        {
            Id = v.Id
        };
    }
    
    public async Task<List<Cliente>> ListarClientes(string nome)
    {
        return await Cliente.ListaClientes(nome);
    }
    
    public async Task<List<Vendedor>> ListarVendedores(string nome)
    {
        return await Vendedor.ListarVendedores(nome);
    }

    public async Task<List<Produto>> ListarProdutos(string nome)
    {
        return await Produto.ListarProdutos(nome);
    }

    public async Task<List<Venda>> CriarVendas(List<VendaRequest> vendas)
    {
        using var context = PottencialContext.Get();

        var result = new List<Venda>();
        vendas.ForEach(v =>
        {
            result.Add(new Venda(v));
        });

        await context.SaveChangesAsync();

        return result;
    }

    public async Task<Venda?> ObterVenda(int id)
    {
        return await Venda.ObterVenda(id);
    }

    public async Task<bool> AtualizarVenda(int id, int status)
    {
        using var context = PottencialContext.Get();

        var venda = await Venda.ObterVenda(id);

        switch (venda.Status)
        {
            case StatusVenda.AguardandoPagamento:
                if (status != (int)StatusVenda.AguardandoPagamento && status != (int)StatusVenda.Cancelada)
                    throw new Exception("Status inválido!");
                break;
            case StatusVenda.PagamentoAprovado:
                if (status != (int)StatusVenda.EnviadoParaTransportadora && status != (int)StatusVenda.Cancelada)
                    throw new Exception("Status inválido!");
                break;
            case StatusVenda.EnviadoParaTransportadora:
                if (status != (int)StatusVenda.Entregue)
                    throw new Exception("Status inválido!");
                break;
        }

        venda.Status = (StatusVenda)status;

        await context.SaveChangesAsync();

        return true;
    }
}