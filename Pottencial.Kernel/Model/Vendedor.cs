using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Permissions;
using Microsoft.EntityFrameworkCore;
using Pottencial.Kernel.DTO.Request;

namespace Pottencial.Kernel.Model;

[Table("vendedor")]
public class Vendedor
{
    [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity), Column("id", TypeName = "INT")] public int Id { get; set; } 
    [Column("email", TypeName = "VARCHAR(45)"), Required] public string Email { get; set; } 
    [Column("nome", TypeName = "VARCHAR(45)"), Required] public string Nome { get; set; }
    [Column("cpf", TypeName = "VARCHAR(11)"), Required] public string Cpf { get; set; }
    [Column("telefone", TypeName = "VARCHAR(11)"), Required] public string Telefone { get; set; }
    [InverseProperty("Vendedor")] public List<Venda> Vendas { get; set; }
    
    public Vendedor(VendedorRequest vendedor)
    {
        Email = vendedor.Email;
        Nome = vendedor.Nome;
        Cpf = vendedor.Cpf;
        Telefone = vendedor.Telefone;
        PottencialContext.Get().VendedorSet.Add(this);
    }
    
    public Vendedor()
    {
    }
    
    public static Task<List<Vendedor>> ListarVendedores(string nome)
    {
        return PottencialContext.Get().VendedorSet.Where(a => a.Nome.Contains(nome)).ToListAsync();
    }
}