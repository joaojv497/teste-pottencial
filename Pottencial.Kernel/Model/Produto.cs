using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Pottencial.Kernel.DTO.Request;

namespace Pottencial.Kernel.Model;

[Table("produto")]
public class Produto
{
    [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity), Column("id", TypeName = "INT")] public int Id { get; set; } 
    [Column("nome", TypeName = "VARCHAR(45)"), Required] public string Nome { get; set; } 
    [Column("marca", TypeName = "VARCHAR(45)"), Required] public string Marca { get; set; } 
    [Column("preco", TypeName = "DOUBLE"), Required] public double Preco { get; set; } 
    [Column("quantidade", TypeName = "INT"), Required] public int Quantidade { get; set; }
    [Column("descricao", TypeName = "TEXT")] public string Descricao { get; set; }

    public Produto(ProdutoRequest produto)
    {
        Nome = produto.Nome;
        Marca = produto.Marca;
        Preco = produto.Preco;
        Quantidade = produto.Quantidade;
        Descricao = produto.Descricao;
        PottencialContext.Get().ProdutoSet.Add(this);
    }

    public Produto()
    {
    }

    public static Task<List<Produto>> ListarProdutos(string nome)
    {
        return PottencialContext.Get().ProdutoSet.Where(a => a.Nome.Contains(nome)).ToListAsync();
    }
}