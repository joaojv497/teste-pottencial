using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Pottencial.Kernel.DTO.Request;
using Pottencial.Kernel.DTO.Response;

namespace Pottencial.Kernel.Model;

[Table("cliente")]
public class Cliente
{
    [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity), Column("id", TypeName = "INT")] public int Id { get; set; } 
    [Column("nome", TypeName = "VARCHAR(45)"), Required] public string Nome { get; set; } 
    [Column("cpf", TypeName = "VARCHAR(11)"), Required] public string Cpf { get; set; } 
    [Column("idade", TypeName = "INT")] public int Idade { get; set; } 
    [Column("telefone", TypeName = "VARCHAR(11)")] public string Telefone { get; set; }
    [InverseProperty("Cliente")] public List<Venda> Vendas { get; set; }

    public Cliente(ClienteRequest cliente)
    {
        Nome = cliente.Nome;
        Cpf = cliente.Cpf;
        Idade = cliente.Idade;
        Telefone = cliente.Telefone;
        
        PottencialContext.Get().ClienteSet.Add(this);
    }

    public Cliente()
    {
        
    }

    public static Task<List<Cliente>> ListaClientes(string nome)
    {
        return PottencialContext.Get().ClienteSet.Where(a => a.Nome.Contains(nome)).ToListAsync();
    }
}