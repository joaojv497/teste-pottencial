using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Pottencial.Kernel.DTO.Enum;
using Pottencial.Kernel.DTO.Request;

namespace Pottencial.Kernel.Model;

[Table("venda")]
public class Venda
{
    [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity), Column("id", TypeName = "INT")] public int Id { get; set; } 
    [Column("vl_total", TypeName = "DOUBLE"), Required] public double VlTotal { get; set; } 
    [Column("status", TypeName = "INT"), Required] public StatusVenda Status { get; set; }
    [Column("id_vendedor", TypeName = "INT"), ForeignKey("Vendedor")] public int IdVendedor { get; set; }
    [Column("id_cliente", TypeName = "INT"), ForeignKey("Cliente")] public int IdCliente { get; set; }
    [InverseProperty("Venda")] public List<VendaItem> VendaItemList { get; set; }  

    public Vendedor Vendedor { get; set; }
    public Cliente Cliente { get; set; }

    public Venda(VendaRequest venda)
    {
        Status = venda.Status;
        IdCliente = venda.IdCliente;
        IdVendedor = venda.IdVendedor;

        VendaItemList = new List<VendaItem>();
        
        venda.Itens.ForEach(a => VendaItemList.Add(new VendaItem()
        {
            Quantidade = a.Quantidade,
            IdProduto = a.IdProduto,
            VlUnidade = a.VlUnidade,
            VlTotal = a.Quantidade * a.VlUnidade
        }));

        VlTotal = Math.Round(VendaItemList.Aggregate(0.0, (ac, next) => ac + next.VlTotal), 2);

        PottencialContext.Get().VendaSet.Add(this);
    }

    public Venda()
    {
    }

    public static Task<Venda?> ObterVenda(int id)
    {
        return PottencialContext.Get().VendaSet.Where(a => a.Id == id).FirstOrDefaultAsync();
    }
}