using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pottencial.Kernel.Model;

[Table("venda_item")]
public class VendaItem
{
    [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity), Column("id", TypeName = "INT")] public int Id { get; set; } 
    [Column("id_venda", TypeName = "INT"), ForeignKey("Venda")] public int IdVenda { get; set; } 
    [Column("id_produto", TypeName = "INT"), ForeignKey("Produto")] public int IdProduto { get; set; } 
    [Column("vl_unidade", TypeName = "DOUBLE"), Required] public double VlUnidade { get; set; } 
    [Column("quantidade", TypeName = "INT"), Required] public int Quantidade { get; set; }
    [Column("vl_total", TypeName = "DOUBLE"), Required] public double VlTotal { get; set; }
    public Venda Venda { get; set; }
    public Produto Produto { get; set; }
    
    
    public VendaItem()
    {
        PottencialContext.Get().VendaItemSet.Add(this);
    }
}