using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using DbContext = Microsoft.EntityFrameworkCore.DbContext;

namespace Pottencial.Kernel.Model;

public class PottencialContext : DbContext
{
    private static AsyncLocal<PottencialContext> _instance = new ();
    internal static PottencialContext Get()
    {
        if (_instance.Value is null)
        {
            var options = new DbContextOptions<PottencialContext>();
            _instance.Value = new PottencialContext(options);
        }
        
        return _instance.Value;
    }
    internal static void ResetContext(){
        _instance.Value = null;
    }

    public PottencialContext(DbContextOptions<PottencialContext> options) : base(options)
    {
    }

    public DbSet<Vendedor> VendedorSet { get; set; } = null!;
    public DbSet<Cliente> ClienteSet { get; set; } = null!;
    public DbSet<Produto> ProdutoSet { get; set; } = null!;
    public DbSet<Venda> VendaSet { get; set; } = null!;
    public DbSet<VendaItem> VendaItemSet { get; set; } = null!;
    

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        if (!optionsBuilder.IsConfigured)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();
            
            var connectionString = configuration.GetConnectionString("MySql");
            optionsBuilder.UseMySql(connectionString, ServerVersion.Parse("8.0.27-mysql"));
        }
    }
    
    public override void Dispose()
    {
        base.Dispose();
        _instance.Value = null;
    }

}