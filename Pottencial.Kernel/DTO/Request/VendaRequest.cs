using Pottencial.Kernel.DTO.Enum;

namespace Pottencial.Kernel.DTO.Request;
public class VendaRequest
{
    public int IdVendedor { get; set; }
    public int IdCliente { get; set; }
    public StatusVenda Status { get; set; }
    public List<VendaItemRequest> Itens { get; set; }
}