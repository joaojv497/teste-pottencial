namespace Pottencial.Kernel.DTO.Request;

public class ProdutoRequest
{
    public string Nome { get; set; }
    public string Marca { get; set; }
    public double Preco { get; set; }
    public int Quantidade { get; set; }
    public string Descricao { get; set; }
    
}