namespace Pottencial.Kernel.DTO.Request;

public class VendaItemRequest
{
    public int IdProduto { get; set; }
    public int Quantidade { get; set; }
    public double VlUnidade { get; set; }
}