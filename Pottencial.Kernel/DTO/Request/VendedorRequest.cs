namespace Pottencial.Kernel.DTO.Request;

public class VendedorRequest
{
    public string Email { get; set; }
    public string Nome { get; set; }
    public string Cpf { get; set; }
    public string Telefone { get; set; }
}