namespace Pottencial.Kernel.DTO.Request;

public class ClienteRequest
{
    public string Nome { get; set; } 
    public string Cpf { get; set; } 
    public int Idade { get; set; } 
    public string Telefone { get; set; }
}