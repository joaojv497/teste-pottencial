using Microsoft.AspNetCore.Mvc;
using Pottencial.Kernel.Controller;
using Pottencial.Kernel.DTO.Request;
using Pottencial.Kernel.DTO.Response;
using Pottencial.Kernel.Model;

namespace Facade.Facede;

[ApiController]    
[Route("api/v1/pottencial", Name = "Pottencial")]	
[Produces("application/json")]
public class OperationFacade : FacadeBase
{
    [HttpPost, Route("CriarCliente")] 
    [ProducesResponseType(typeof(ClienteResponse), StatusCodes.Status200OK)] 
    public async Task<ClienteResponse> CriarCliente([FromBody] ClienteRequest cliente)
    {
        return await new OperationController().CriarCliente(cliente);
    }
    
    [HttpPost, Route("CriarVendedor")] 
    [ProducesResponseType(typeof(VendedorResponse), StatusCodes.Status200OK)] 
    public async Task<VendedorResponse> CriarVendedor([FromBody] VendedorRequest vendedor)
    {
        return await new OperationController().CriarVendedor(vendedor);
    }
    
    [HttpPost, Route("CriarProduto")] 
    [ProducesResponseType(typeof(ProdutoResponse), StatusCodes.Status200OK)] 
    public async Task<ProdutoResponse> CriarProduto([FromBody] ProdutoRequest produto)
    {
        return await new OperationController().CriarProduto(produto);
    }
    
    [HttpGet, Route("ListarClientes")] 
    [ProducesResponseType(typeof(List<Cliente>), StatusCodes.Status200OK)] 
    public async Task<List<Cliente>> ListarClientes([FromQuery] string nome)
    {
        return await new OperationController().ListarClientes(nome);
    }
    
    [HttpGet, Route("ListarVendedores")] 
    [ProducesResponseType(typeof(List<Vendedor>), StatusCodes.Status200OK)] 
    public async Task<List<Vendedor>> ListarVendedores([FromQuery] string nome)
    {
        return await new OperationController().ListarVendedores(nome);
    }
    
    [HttpGet, Route("ListarProdutos")] 
    [ProducesResponseType(typeof(List<Produto>), StatusCodes.Status200OK)] 
    public async Task<List<Produto>> ListarProdutos([FromQuery] string nome)
    {
        return await new OperationController().ListarProdutos(nome);
    }

    [HttpPost, Route("CriarVendas")] 
    [ProducesResponseType(typeof(List<Venda>), StatusCodes.Status200OK)] 
    public async Task<List<Venda>> CriarVendas(List<VendaRequest> vendas)
    {
        return await new OperationController().CriarVendas(vendas);
    }
    
    [HttpGet, Route("ObterVenda")] 
    [ProducesResponseType(typeof(Venda), StatusCodes.Status200OK)] 
    public async Task<Venda?> ObterVenda([FromQuery] int id)
    {
        return await new OperationController().ObterVenda(id);
    }
    
    [HttpPost, Route("AtualizarVenda")] 
    [ProducesResponseType(typeof(bool), StatusCodes.Status200OK)] 
    public async Task<bool> AtualizarVenda([FromQuery] int id, int status)
    {
        return await new OperationController().AtualizarVenda(id, status);
    }
}